# TpUser

lien de la page web : http://webpage-p2100284-c50744b0388012df24adfb8a01ca8780767032272779dc.pages.univ-lyon1.fr/

## Login

login : `admin`
password : `Passpass2@`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `public/` directory.