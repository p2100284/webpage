import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { AddUserComponent } from './edit-user/add-user/add-user.component';
import { UpdateUserComponent } from './edit-user/update-user/update-user.component';
import { LoginPageComponent } from './login-page/login-page.component';

const routes: Routes = [
  {
    path: "",
    component: UserListComponent
  },
  { path: 'list', redirectTo: "", pathMatch: "full" },

  { path: "user/:id", component: UserDetailComponent },
  { path: "edit/:id", component: UpdateUserComponent },
  { path: "add", component: AddUserComponent },
  { path: "login", component: LoginPageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
