import { Injectable } from '@angular/core';
import { LoginInfo } from '../models/login-info.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserAuthentifactionService {

  isConnected = new BehaviorSubject<boolean>(false);

  constructor() { }


  connect(loginInfo: LoginInfo): boolean {
    if (loginInfo.login === "admin" && loginInfo.password === "Passpass2@") {
      this.isConnected.next(true);
      localStorage.setItem("is_connected", "true");
      return true;
    }
    return false;
  }



  disconnect() {
    this.isConnected.next(false);
  }

}
