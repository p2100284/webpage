import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from '../models/user.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserHttpService {
  constructor(private httpClient: HttpClient) { }

  baseUrl = "https://6569900dde53105b0dd73eb7.mockapi.io/api/user";

  public getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUrl);
  }


  public getUser(id: string): Observable<User> {
    return this.httpClient.get<User>(this.baseUrl + "/" + id);
  }

  public addUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.baseUrl, user);
  }

  public delteUser(id: string): Observable<any> {
    return this.httpClient.delete(this.baseUrl + "/" + id, {
      responseType: 'text'
    });
  }

  public updateUser(id: string, user: User): Observable<any> {
    return this.httpClient.put(this.baseUrl + "/" + id, user);
  }
}
