import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { UserHttpService } from './user-http.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  public users = new BehaviorSubject<User[]>([]);

  constructor(private userHttp: UserHttpService) {
    this.loadUserInCache();
  }



  private loadUserInCache() {
    return this.getUsers().subscribe(usersList => {
      this.users.next(usersList);
      console.debug("user loaded");
    })
  }

  private getUsers(): Observable<User[]> {
    return this.userHttp.getUsers();
  }

  getUser(id: string): Observable<User> {
    return this.userHttp.getUser(id);
  }

  addUser(user: User) {
    return this.userHttp.addUser(user).subscribe(ass => {
      this.loadUserInCache().add(() => {
        console.info("add usersList");
      });
    });

  }


  deleteUser(user: User): Subscription {
    return this.userHttp.delteUser(user.id.toString()).subscribe(() => {
      this.loadUserInCache().add(() => {
        console.info("delete one user");
      });
    })
  }





  editUser(id: number, newUser: User) {
    return this.userHttp.updateUser(id.toString(), newUser).subscribe(() => {

      this.loadUserInCache().add(() => {
        console.info("edit one user");
      });
    })
  }
}
