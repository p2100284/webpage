import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: '../edit-user.component.html',
  styleUrl: '../edit-user.component.scss'
})
export class AddUserComponent implements OnInit {

  user: User = new User("", "", "", "");
  hidePassword: boolean = true;

  SubmitButtonLabel = "Add";

  emailControl = new FormControl('', [Validators.required, Validators.email]);
  getErrorEmailMessage() {
    return this.emailControl.hasError('required') ? 'You must enter a value' :
      this.emailControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  nameControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorNameMessage() {
    return this.nameControl.invalid ? 'You must enter a value (3 character min.)' : '';
  }
  occupationControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorOccupationMessage() {
    return this.occupationControl.invalid ? 'You must enter a value (3 character min.)' :
      '';
  }
  bioControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorBioMessage() {
    return this.bioControl.invalid ? 'You must enter a value (3 character min.)' :
      '';
  }



  constructor(private router: Router, private userService: UserService) { }



  ngOnInit() {

  }

  onSubmit() {
    if (this.emailControl.valid && this.nameControl.valid && this.occupationControl.valid && this.bioControl.valid) {
      this.userService.addUser(this.user);
      this.router.navigate([""]);
    }
  }

  onClickBack() {
    this.router.navigate([""]);
  }
}
