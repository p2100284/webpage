import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: '../edit-user.component.html',
  styleUrl: '../edit-user.component.scss'
})
export class UpdateUserComponent {

  user: User = new User("", "", "", "");
  hidePassword: boolean = true;

  SubmitButtonLabel = "Update";

  emailControl = new FormControl('', [Validators.required, Validators.email]);
  getErrorEmailMessage() {
    return this.emailControl.hasError('required') ? 'You must enter a value' :
      this.emailControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  nameControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorNameMessage() {
    return this.nameControl.invalid ? 'You must enter a value (3 character min.)' : '';
  }
  occupationControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorOccupationMessage() {
    return this.occupationControl.invalid ? 'You must enter a value (3 character min.)' :
      '';
  }
  bioControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorBioMessage() {
    return this.bioControl.invalid ? 'You must enter a value (3 character min.)' :
      '';
  }




  constructor(private router: Router, private userService: UserService, private route: ActivatedRoute) { }



  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.userService.getUser(id).subscribe(user => this.user = user);
      }
    })
  }

  onSubmit() {
    if (this.emailControl.valid && this.nameControl.valid && this.occupationControl.valid && this.bioControl.valid) {


      this.userService.editUser(this.user.id, this.user);
      this.router.navigate([""]);
    }
  }
  onClickBack(){
    this.router.navigate([""]);
  }
}
