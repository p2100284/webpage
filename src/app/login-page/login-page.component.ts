import { Component } from '@angular/core';
import { LoginInfo } from '../models/login-info.model';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserAuthentifactionService } from '../shared/user-authentifaction.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrl: './login-page.component.scss'
})
export class LoginPageComponent {
  loginInfo: LoginInfo = new LoginInfo("", "");
  hidePassword: boolean = true;


  constructor(private router: Router, private snackBar: MatSnackBar, private authService: UserAuthentifactionService) { }

  loginControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  getErrorNameMessage() {
    return this.loginControl.invalid ? 'You must enter a value (3 character min.)' : '';
  }

  passwordControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,}$/)]);
  getErrorPasswordMessage() {
    return this.passwordControl.invalid ? 'You must enter a value (8 character min., lowercase, uppercase, digit and special character)' : '';
  }


  onClickBack(event: MouseEvent) {
    event.preventDefault();
    this.router.navigate([""]);
  }
  onSubmit() {
    if (this.loginControl.valid && this.passwordControl.valid) {
      if (this.authService.connect(this.loginInfo)) {
        this.router.navigate([""]);
      } else {
        this.snackBar.openFromComponent(this.snackBar.simpleSnackBarComponent, {
          duration: 1000,
          data: {
            message: "Wrong password or login"
          }
        })
      }
    }
  }
}
