import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthentifactionService } from './shared/user-authentifaction.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tp_user';

  isConnected = false;

  constructor(private router: Router, private authService: UserAuthentifactionService) {
    authService.isConnected.subscribe(connected => this.isConnected = connected);

  }

  goLogin() {
    this.router.navigate(["login"]);
  }

  disconnected() {
    this.authService.disconnect();
  }

}
