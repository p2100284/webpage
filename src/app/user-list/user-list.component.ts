import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';


import { User } from '../models/user.model';
import { UserService } from '../shared/user.service';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserAuthentifactionService } from '../shared/user-authentifaction.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements AfterViewInit {

  displayedColumns: string[] = ['name', 'email', 'occupation', "detail", "update", "delete"];

  EmpData: User[] = [];

  featureUpdateAndDeleteEnabled = false;

  dataSource = new MatTableDataSource(this.EmpData);


  @ViewChild('empTbSort') empTbSort = new MatSort();
  @ViewChild("matPaginator") paginator!: MatPaginator;



  constructor(private userService: UserService, private router: Router, private snackBar: MatSnackBar,
    private authService: UserAuthentifactionService) {

  }
  ngAfterViewInit(): void {
    this.loadData();

    this.authService.isConnected.subscribe(connected => {
      this.featureUpdateAndDeleteEnabled = connected;

    })
  }


  applyFilter(event: Event) {
    let filterValue = (event.target as HTMLInputElement).value;
    filterValue ??= "";

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;

  }


  loadData() {
    this.userService.users.subscribe(newUsers => {
      this.dataSource = new MatTableDataSource(newUsers);
      this.setupFeature();
    });
  }



  setupFeature() {
    //sort feature
    this.empTbSort.disableClear = true;
    this.dataSource.sort = this.empTbSort;


    //paginator feature
    this.dataSource.paginator = this.paginator;
  }



  onClickDetail(user: User) {
    this.router.navigate([`/user/${user.id}`])
  }
  onClickUpdate(user: User) {
    this.router.navigate([`/edit/${user.id}`])
  }

  onClickDelete(user: User) {
    this.snackBar.openFromComponent(this.snackBar.simpleSnackBarComponent, {
      duration: 500,
      data: {
        message: "Removing user..."
      }
    })
    this.userService.deleteUser(user);
  }


  onClickAdd() {
    this.router.navigate(["add"])
  }
}
