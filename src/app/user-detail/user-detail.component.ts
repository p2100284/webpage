import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { User } from '../models/user.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrl: './user-detail.component.scss'
})
export class UserDetailComponent implements OnInit {

  user: User | null = null;

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.userService.getUser(id).subscribe(user => this.user = user);
      }
    })
  }


  goBack() {
    this.router.navigate([""])
  }


}
