export class User {
	id!: number;
	name!: string;
	occupation!: string;
	email!: string;
	bio!: string;

	constructor(name : string, occupation: string, email: string, bio: string){
		this.name = name;
		this.occupation = occupation;
		this.email = email;
		this.bio = bio;
	}
}